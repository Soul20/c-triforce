# C Triforce

C program to creates a triforce pattern

## Usage
./a.out \<int total-layers\> \<int triangle-width\>
<pre>./a.out 4 9
                                             A        
                                            AAA       
                                           AAAAA      
                                          AAAAAAA     
                                         AAAAAAAAA    
                                        AAAAAAAAAAA   
                                       AAAAAAAAAAAAA  
                                      AAAAAAAAAAAAAAA 
                                     AAAAAAAAAAAAAAAAA
                                    A                 A        
                                   AAA               AAA       
                                  AAAAA             AAAAA      
                                 AAAAAAA           AAAAAAA     
                                AAAAAAAAA         AAAAAAAAA    
                               AAAAAAAAAAA       AAAAAAAAAAA   
                              AAAAAAAAAAAAA     AAAAAAAAAAAAA  
                             AAAAAAAAAAAAAAA   AAAAAAAAAAAAAAA 
                            AAAAAAAAAAAAAAAAA AAAAAAAAAAAAAAAAA
                           A                 A                 A        
                          AAA               AAA               AAA       
                         AAAAA             AAAAA             AAAAA      
                        AAAAAAA           AAAAAAA           AAAAAAA     
                       AAAAAAAAA         AAAAAAAAA         AAAAAAAAA    
                      AAAAAAAAAAA       AAAAAAAAAAA       AAAAAAAAAAA   
                     AAAAAAAAAAAAA     AAAAAAAAAAAAA     AAAAAAAAAAAAA  
                    AAAAAAAAAAAAAAA   AAAAAAAAAAAAAAA   AAAAAAAAAAAAAAA 
                   AAAAAAAAAAAAAAAAA AAAAAAAAAAAAAAAAA AAAAAAAAAAAAAAAAA
                  A                 A                 A                 A        
                 AAA               AAA               AAA               AAA       
                AAAAA             AAAAA             AAAAA             AAAAA      
               AAAAAAA           AAAAAAA           AAAAAAA           AAAAAAA     
              AAAAAAAAA         AAAAAAAAA         AAAAAAAAA         AAAAAAAAA    
             AAAAAAAAAAA       AAAAAAAAAAA       AAAAAAAAAAA       AAAAAAAAAAA   
            AAAAAAAAAAAAA     AAAAAAAAAAAAA     AAAAAAAAAAAAA     AAAAAAAAAAAAA  
           AAAAAAAAAAAAAAA   AAAAAAAAAAAAAAA   AAAAAAAAAAAAAAA   AAAAAAAAAAAAAAA 
          AAAAAAAAAAAAAAAAA AAAAAAAAAAAAAAAAA AAAAAAAAAAAAAAAAA AAAAAAAAAAAAAAAAA
</pre>
