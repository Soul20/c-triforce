#include "stdio.h"
#include "stdlib.h"

void render (char c, int l){
    for (int i=0; i<l; i++) printf("%c", c);
}

int main(int argc, char ** argv){

    int totalLayers = atoi(argv[1]), triangleSize = atoi(argv[2]);
    int blockWidth = (triangleSize*2) + 1;

    char airChar = ' ', brickChar = 'A';

    for (int i=0; i<totalLayers; i++){
        for (int ii=0; ii<triangleSize; ii++){

            int offset = (totalLayers - i) * triangleSize;
            render(airChar, offset);

            for (int _=0; _<i+1; _++){
                int padding = (blockWidth - ((ii*2)+1))/2;
                render(airChar, padding);
                render(brickChar, (ii*2)+1);
                render(airChar, padding - 1);
            }
            printf("\n");
        }
    }
    return 0;
}
